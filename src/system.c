#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "zlib.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>



void fortran_mkdir(char* filename) {
  if (mkdir (filename, S_IRWXU)) {
     perror (strerror (errno));
  }
}


#define BUFSIZE 16384
void gzip(char* filename, char* output) {
  FILE* in  = fopen(filename,"r");
  gzFile out = gzopen(output,"w");
  char buffer[BUFSIZE];
  unsigned int size;
  while (!feof(in)) {
     size = (unsigned int) fread (buffer, sizeof(char), BUFSIZE, in);
     gzwrite(out, buffer, size);
  }
  fclose(in);
  gzclose(out);
}

void zcat(char* filename, char* output) {
  gzFile in = gzopen(filename,"r");
  FILE* out = fopen(output,"w");
  char buffer[BUFSIZE];
  int size;
  while (!gzeof(in)) {
     size = gzread (in, buffer, BUFSIZE);
     fwrite(buffer, sizeof(char), (size_t) size, out);
  }
  gzclose(in);
  fclose(out);
}

